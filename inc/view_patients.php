<!-- breadcrumb -->
<nav aria-label='breadcrumb'>
  <ol class='breadcrumb'>
    <li class='breadcrumb-item active' aria-current='page'><a href='master.php?page=records'>Home</a></li>
  </ol>
</nav>

<div class="jumbotron jumbotron-fluid bg-light">
  <div class="container">
    <h1 class="display-4">View of all patients</h1>
    <p class="lead">You may search for patients by name.</p>
  </div>
</div>
<div class = "container-sm">
<div class="input-group mb-3 text-center">
  <input title="type the name of the patient you wish to search for." type="text" class="form-control" placeholder="Search patient by first or surname" aria-label="Search patient by name" id= "search_data" aria-describedby="button-addon2" method="post">
</div>
</div>
<div class="container">
<table class="table" id="autodata">

</table>
</div>
<script>
$("#autodata").load("inc/getdata.php"); // inital load

var search= ""; // placeholder 
$(document).ready(function() {
 setInterval(function() { //runs this code after set interval
      if($("#search_data").val() === "") { //if search data is empty
      $("#autodata").load("inc/getdata.php?search="+search);    //loads page

     } else {
$(document).ready(function(){
  $("#search_data").keyup(function(){ //runs code on keyup
    search = $('#search_data').val() //sets search to #search_data input
    $("#autodata").load("inc/getdata.php?search="+search);//loads data with search criteria 
  });
});
     }
}, 1000);//interval in ms
}); 
</script>
