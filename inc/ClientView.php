<!-- Temp DB Connection and Mangement -->
<?php
function BitToBoolean($bit) {
    if($bit == 0)
        $boolean = "False";
    else $boolean = "True";
    return $boolean;
}
function BitToYesNo($bit) {
    if($bit == 0)
        $boolean = "No";
        else $boolean = "Yes";
        return $boolean;
}

// SQL Server Extension Sample Code:
$connectionInfo = array("UID" => "ljmu", "pwd" => "{EpicGamer69}", "Database" => "covid_application", "LoginTimeout" => 30, "Encrypt" => 1, "TrustServerCertificate" => 0);
$serverName = "tcp:theprocrastinators.database.windows.net,1433";
$conn = sqlsrv_connect($serverName, $connectionInfo);

$sql = "SELECT PatientID, Users.FirstName, Users.Surname, Patients.DoB, NoOfPplInHouse, UseOfPubTrans, Pregnant, Smoker, AdditionalConditions, NoOfHos, Diabetes, Addresses.AddressLine1, Addresses.AddressLine2, Addresses.City, Addresses.County, Addresses.Postcode, Patients.AddressID 
	FROM Patients
	JOIN Users ON Users.UserID = Patients.UserID
    JOIN Addresses ON Addresses.AddressID = Patients.AddressID
    WHERE PatientID = ".$id = $_GET['id'].";";

$stmt = sqlsrv_query($conn, $sql);
if ($stmt === false)
{
    die (print_r( sqlsrc_error(), true));
}

while ($row = sqlsrv_fetch_array($stmt, SQLSRV_FETCH_ASSOC))
{
    $firstname = $row['FirstName'];
    $surname = $row['Surname'];
    $DOB = $row['DoB'];
    $diabetes = BitToYesNo($row['Diabetes']);
    $NumOfHosp = $row['NoOfHos'];
    $NoOfPplInHouse = $row['NoOfPplInHouse'];
    $UseOfPubTrans = BitToYesNo($row['UseOfPubTrans']);
    $pregnant = BitToYesNo($row['Pregnant']);
    $smoker = BitToYesNo($row['Smoker']);
    if ($row['AdditionalConditions']== null)
    {
        $AdCon = "None";
    }
    else
    $AdCon = $row['AdditionalConditions'];
    $addressLine1 = $row['AddressLine1'];
    $addressLine2 = $row['AddressLine2'];
    $city = $row['City'];
    $county = $row['County'];
    $postcode = $row['Postcode'];
}

?>


<?php echo "
<!-- Breadcrumb -->
<nav aria-label='breadcrumb'>
  <ol class='breadcrumb'>
    <li class='breadcrumb-item'><a href='master.php?page=records'>Home</a></li>
    <li class='breadcrumb-item active' aria-current='page'>Client View: ".$firstname." ".$surname."</li>
  </ol>
</nav>

  <!-- Page Header -->
    <div class='jumbotron jumbotron-fluid'>
  <div class='container'>
    <h1 class='display-4'>Client View</h1>
    <p class='lead'>View Previous and Current Client Symptom Reports for this spesific User as well as their User Details</p>
  </div>
</div>

<!-- Current Client Details-->
<div class='container-fluid'>
<div class='row'>
  <div class='col-sm-6'>
    <div class='card' style='margin-top: 5px'>
      <div class='card-body'>
         <h5 class='card-title'>Client Details</h5>
        <p class='card-text'>Patient Name: ".$firstname." ".$surname."</p>
        <p class='card-text'>Date of Birth: ".date_format($DOB, 'd/m/Y')."</p>
        <p class='card-text'>Diabetes: ".$diabetes."</p>
        <p class='card-text'>Number of Hospitalisations: ".$NumOfHosp."</p>
        <p class='card-text'>Number of members in household: ".$NoOfPplInHouse."</p>
        <p class='card-text'>Use of Public Transport: ".$UseOfPubTrans."</p>
        <p class='card-text'>Pregnant: ".$pregnant."</p>
        <p class='card-text'>Smoker: ".$smoker."</p>
        <p class='card-text'>Additional Conditions: ".$AdCon."</p>
        <h6 class='card-title'>Client Address</h6>
        <p class='card-text'>".$addressLine1."</p>
        <p class='card-text'>".$addressLine2."</p>
        <p class='card-text'>Postcode: ".$postcode."</p>
        <p class='card-text'>City: ".$city."</p>
        <p class='card-text'>County: ".$county."</p>
    </div>
  </div>
    </div>
<div class='col-sm-6' id=x>
</div>
<div class='col mb-4' style='margin-top: 15px' id=y>
</div>
";
?>
<script>
$(document).ready(function() {
    setInterval(function() {
        $("#x").load("inc/getClientViewLatestReport.php?id=<?php echo $_GET['id'];?>");
    }, 1000);
});
$(document).ready(function() {
    setInterval(function() {
        $("#y").load("inc/getClientViewAllReports.php?id=<?php echo $_GET['id'];?>");
    }, 1000);
});
</script>
