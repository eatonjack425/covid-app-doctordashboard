<?php
$connectionInfo = array("UID" => "ljmu", "pwd" => "EpicGamer69", "Database" => "covid_application", "LoginTimeout" => 30, "Encrypt" => 1, "TrustServerCertificate" => 0);
$serverName = "tcp:theprocrastinators.database.windows.net,1433";
$conn = sqlsrv_connect($serverName, $connectionInfo);
$search;
    $sql = "SELECT patients.patientid, users.FirstName, users.Surname, patients.DoB, patients.NoOfPplInHouse, patients.UseOfPubTrans, patients.NoOfHos, patients.Diabetes
FROM users INNER JOIN patients ON users.UserId = patients.UserId ";
if(isset($_GET["search"])){
$search = htmlspecialchars($_GET["search"]);
$sql = $sql ."WHERE users.FirstName LIKE '%".$search."%' OR users.surname LIKE '%".$search."%';";
}
$stmt = sqlsrv_query($conn, $sql);
if( $stmt === false) {
    die( print_r( sqlsrv_errors(), true) );
}

echo '<thead class="thead-info">
    <tr>
      <th title="Name of patient." scope="col">Name</th>
      <th title="Date of birth of patient." scope="col">Date of birth</th>
      <th title="Number in household." scope="col">Number in household</th>
      <th title="Use of public transport." scope="col">Use of public transport</th>
      <th title="number of times the patient has been hositalized."scope="col">Number of Hospitalizations</th>
      <th title="If the patient has diabetes type 1 or 2."scope="col">Diabetes</th>
    </tr>
  </thead>';

while( $row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC) ) {
      if($row["Diabetes"]== 0){
        $diabetes = "No";
      }
      else{
        $diabetes = "Yes";
      }
      if($row["UseOfPubTrans"]== 0){
        $Bus = "No";
      }
      else{
        $Bus = "Yes";
      }
      echo 
        '<tbody>
      <tr>
      <th scope="row" title="Click to go to patients information"><a href="master.php?page=clientview&id='.$row["patientid"].'">'.$row["FirstName"].' '.$row["Surname"].'</a></th>
      <td>'.$row["DoB"]->format('d/m/Y').'</td>
      <td>'.$row["NoOfPplInHouse"].'</td>
      <td>'.$Bus.'</td>
      <td>'.$row["NoOfHos"].'</td>
      <td>'.$diabetes.'</td>
    </tr>
    </tbody>';
}
?>