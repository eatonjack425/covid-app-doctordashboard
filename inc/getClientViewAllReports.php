<?php 
function BitToBoolean($bit) {
    if($bit == 0)
        $boolean = "False";
        else $boolean = "True";
        return $boolean;
}
function BitToYesNo($bit) {
    if($bit == 0)
        $boolean = "No";
        else $boolean = "Yes";
        return $boolean;
}
?>
<div class='col mb-4' style='margin-top: 15px'>
<div class='card'>
<div class='table-responsive-sm'>
  <table class='table'>
  <thead class='thead-info'>
    <tr>
      <th title="Date and time of patient record." scope='col'>DateTime</th>
      <th title="Temprature of patient.(should be around 37°C)" scope='col'>Temperature</th>
      <th title="Stress level of patient." scope='col'>Stress Levels</th>
      <th title="Symptoms like; fever, fatuige, cough, sore throat, headache, difficulty sleeping, stomach issues and feeling sick" scope='col'>Flu Like Symptoms</th>
      <th title="High blood pressure." scope='col'>Hypertension</th>
      <th title="Current weight of patient." scope='col'>Weight</th>
      <th title="Coughing for more than up to 2-3hours." scope='col'>Continuous Cough</th>
      <th title="Loss of taste or smell(very big COVID19 tell)" scope='col'>Loss Of Taste</th>
      <th title="Breathing issues also big tell of COVID19" scope='col'>Short Of Breath</th>
      <th title="Lost the need to eat/does not feel hungry." scope='col'>Loss Of Appetite</th>
    </tr>
  </thead>
  <tbody>
  <?php
  $connectionInfo = array("UID" => "ljmu", "pwd" => "{EpicGamer69}", "Database" => "covid_application", "LoginTimeout" => 30, "Encrypt" => 1, "TrustServerCertificate" => 0);
  $serverName = "tcp:theprocrastinators.database.windows.net,1433";
  $conn = sqlsrv_connect($serverName, $connectionInfo);
  
  $sql = "SELECT RecordID, DateTimeOfRecord, Temp, StressLevel, Weight, ContCough, FluLike, LossOfTaste, LossOfTaste, Hypertension, ShortOfBreath, LossOfAppetite
	FROM Records
    WHERE PatientID = ".$_GET['id'].";";

$stmt = sqlsrv_query($conn, $sql);
if ($stmt === false)
{
    die (print_r( sqlsrc_error(), true));
}

while ($row = sqlsrv_fetch_array($stmt, SQLSRV_FETCH_ASSOC))
{
      $ContCough =  BitToYesNo($row["ContCough"]);
      $FluLike =  BitToYesNo($row["FluLike"]);
      $LossOfTaste =  BitToYesNo($row["LossOfTaste"]);
      $ShortOfBreath = BitToYesNo($row["ShortOfBreath"]);
      $LossOfAppetite = BitToYesNo($row["LossOfAppetite"]);
      $Hypertension = BitToYesNo($row["Hypertension"]);
    echo "<tr>
    <th scope='row'>".date_format($row['DateTimeOfRecord'], 'H:i:s d/m/Y')."</th>
    <td>".$row['Temp']."°C</td>
    <td>".$row['StressLevel']."/10</td>
    <td>".$FluLike."</td>
    <td>".$Hypertension."</td>
    <td>".$row['Weight']." kg</td>
    <td>".$ContCough."</td>
    <td>".$LossOfTaste."</td>
    <td>".$ShortOfBreath."</td>
    <td>".$LossOfAppetite."</td>
    </tr>";
}

echo "
    </tbody>
</table>
</div>
</div>
</div>
";
?>
